# OpenML dataset: 2dplanes

https://www.openml.org/d/215

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown -   
**Please cite**:   

This is an artificial data set described in Breiman et al. (1984,p.238) 
 (with variance 1 instead of 2).  
 
 Generate the values of the 10 attributes independently
 using the following probabilities:

 P(X_1 = -1) = P(X_1 = 1) = 1/2
 P(X_m = -1) = P(X_m = 0) = P(X_m = 1) = 1/3, m=2,...,10

 Obtain the value of the target variable Y using the rule:

 if X_1 = 1 set Y = 3 + 3X_2 + 2X_3 + X_4 + sigma(0,1)
 if X_1 = -1 set Y = -3 + 3X_5 + 2X_6 + X_7 + sigma(0,1)

 Characteristics: 40768 cases, 11 continuous attributes
 Source: collection of regression datasets by Luis Torgo (ltorgo@ncc.up.pt) at
 http://www.ncc.up.pt/~ltorgo/Regression/DataSets.html
 Original source: Breiman et al. (1984, p.238).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/215) of an [OpenML dataset](https://www.openml.org/d/215). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/215/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/215/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/215/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

